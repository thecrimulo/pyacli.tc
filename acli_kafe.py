# Kafe module for Anvil CLI
# Functions are built here
# You may not the functions
# by thecrimulo

## Import
import acli_utils
import ftplib
import os

## FTPLib wrapper
class Kafe:
    # Use: 'kafe fetch package'
    # kafe fetch kafe-test
    def fetch(self, pkgname):
        with ftplib.FTP("") as ftp:
            ftp.connect("kernl.myftp.org", 24021)
            ftp.login("pkguser", "kacli")
            ftp.cwd("/pkgstor/" + pkgname + "/")
            filename = "cinsert.kci"
            localfile = open("./pkg/" + filename, 'wb')
            ftp.retrbinary('RETR ' + filename, localfile.write, 1024)

    # Use: 'kafe upload package file'
    # kafe upload kafe-test ./tmp/myownpkg.anv
    def upload(self, pkgname, pkgctn):
        with ftplib.FTP("") as ftp:
            ftp.connect("kernl.myftp.org", 24021)
            ftp.login("pkguser", "kacli")
            ftp.cwd("/pkgstor/")
            ftp.mkd(pkgname)
            ftp.cwd(pkgname + "/")
            os.chdir("./pkg/" + pkgname + "/")
            ftp.storlines("STOR " + pkgctn, open(pkgctn))
            os.rmdir("./pkg/" + pkgname + "/")

    # Use: 'kafe install package'
    # kafe install kafe-test
    def install(self, pkgname):
        reflink = ""
        symlink = ""
        pkgnm = ""
        pkgvr = ""
        lines = [line.rstrip('\n') for line in open('./pkg/' + pkgname + '.ksl')]
        for line in lines:
            if line.startswith("#"):
                continue
            elif line.startswith("@kafe:ref-link : "):
                getp = line.split(" : ")
                reflink = getp[1]
            elif line.startswith("@kafe:sym-link : "):
                getp = line.split(" : ")
                symlink = getp[1]
            elif line.startswith("@kafe:pkg-name : "):
                getp = line.split(" : ")
                pkgnm = getp[1]
            elif line.startswith("@kafe:pkg-vers : "):
                getp = line.split(" : ")
                pkgvr = getp[1]
            else:
                acli_utils.Workflow.error("[Kafe]: Error in Kafe Symlink, wrong formatting.")

        codelines = acli_utils.Curl.get(reflink)
        insrt = open("./pkg/" + pkgname + ".kci", "w+")
        insrt.writelines(codelines)
        insrt.close()
        counter = 0
        counterins = 0
        putfile = [line.rstrip('\n') for line in open('./pkg/' + pkgname + '.kci')]
        aclipkgmod = open("./acli_pkg.py", "r+")
        ## Insert Doer
        for line in aclipkgmod:
            if not line.startswith("#@pkgend : "):
                counter += 1
                continue
            for lien in putfile:
                if not lien.startswith("#@pkgsta : "):
                    counterins += 1
                    continue
                else:
                    while not lien.startswith("#@pkgend : "):
                        counter += 1
                        aclipkgmod.insert(counter, putfile[counterins])
                        counterins += 1
        ## Insert Initializer
        counter = 0
        counterins = 0
        putfileinit = [line.rstrip('\n') for line in open('./pkg/' + pkgname + '.kci')]
        aclipkgmodinit = open("./acli_pkg.py", "r+")
        for line in aclipkgmodinit:
            if not line.startswith("#@initend : "):
                counter += 1
                continue
            for lien in putfileinit:
                if not lien.startswith("#@initsta : "):
                    counterins += 1
                    continue
                else:
                    while not lien.startswith("#@initend : "):
                        counter += 1
                        aclipkgmodinit.insert(counter, putfileinit[counterins])
                        counterins += 1
