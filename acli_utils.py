# Utils for Anvil CLI
# DO NOT MODIFY
#
# by thecrimulo

import pycurl
import random
import os

## ANSI Color Codes
# 30 40	Black
# 31 41 Red
# 32 42	Green
# 33 43	Yellow
# 34 44	Blue
# 35 45	Magenta
# 36 46	Cyan
# 37 47	White

class color:
    black = "\033[0;30;40m"
    red = "\033[0;31;40m"
    green = "\033[0;32;40m"
    yellow = "\033[0;33;40m"
    blue = "\033[0;34;40m"
    magenta = "\033[0;35;40m"
    cyan = "\033[0;36;40m"
    white = "\033[0;37;40m"
class boldcolor:
    black = "\033[1;30;40m"
    red = "\033[1;31;40m"
    green = "\033[1;32;40m"
    yellow = "\033[1;33;40m"
    blue = "\033[1;34;40m"
    magenta = "\033[1;35;40m"
    cyan = "\033[1;36;40m"
    white = "\033[1;37;40m"
class escapes:
    nl = "\n"

class Workflow:
    def error(self, msg):
        print(color.red + msg + color.white)
    def warning(self, msg):
        print(color.yellow + msg + color.white)
    def success(self, msg):
        print(color.green + msg + color.white)

class Curl:
    def get(self, URLS):
        r = random.random()
        f = open("./tmp/curl/" + str(r) + ".ktf", "wb+")
        curl = pycurl.Curl()
        curl.setopt(curl.URL, URLS)
        curl.setopt(curl.WRITEFUNCTION, f.write)
        curl.perform()
        curl.close()
        f.close()
        lines = [line.rstrip('\n') for line in open("./tmp/curl/" + str(r) + ".ktf")]
        os.remove("./tmp/curl/" + str(r) + ".ktf")
        return lines
