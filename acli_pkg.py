# Packages module for Anvil CLI
# Package installers will write here.
#

## Imports
import os

## Tracer
#import pdb; pdb.set_trace()

## Dictionary
cllist = {}

## Doers
#@pkgstr : METAkafe
def METAkafe():
    print("METAkafe: It's meta bois. ~EvilAFM")
#@pkgend : METAkafe


## Initializer
#@initsta : METAkafe
def pkginit():
    cllist["metakafe"] = METAkafe
#@initend : METAkafe


##@EOF : acli_pkg
