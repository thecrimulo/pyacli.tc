# PyaCLI - Anvil CLI in Python
## Basics
PyaCLI, formerly GEP Anvil CLI, is a simple command-line interface program, similar to bash, but less powerful for now. The objective of the project is to make a powerful, advanced and useful CLI that KernlORG can help in a lot of tasks.

## Kafe - Our PM
Kafe, or KernlORG Anvil FTP Environment, is our package manager for this CLI. Simple usage, Upload and Download features

## Hackability
Fully open-source, you can do any modifications if it's for yourself.
Also, if you want to add your own commands, you can upload packages with Kafe

By TheCrimulo

