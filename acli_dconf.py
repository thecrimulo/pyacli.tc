# Function module for Anvil CLI
# Results are preconfigured here
# You can edit the functions
# by thecrimulo

from acli_utils import *
import acli_sconf
import acli_cconf
import acli_kafe
acli_kafepy = acli_kafe.Kafe()
MainLabels = acli_cconf.Main()
ArithmeticLabels = acli_cconf.Arithmetic()

# TODO: Add all functions

class Main:
    def main_help(self):
        print('Anvil CLI Mercury - KernlORG GEP')
        print('GNU GPL v3.0 License : Please do not copy')
        print('==========================================')
        looper = 0
        while looper < len(acli_cconf.cmdls):
            print(acli_cconf.cmdls[looper] + " : " + acli_sconf.sumls[looper])
            looper += 1

    def main_helpcmd(self, cmd):
        print(cmd, " : ",acli_sconf.sumdict[cmd])

    def main_exit(self):
        exit()

class Arithmetic:
    def arith_addition(self, a, b, c=0, d=0, e=0, f=0):
        if a == None or b == None:
            Workflow.error("This command requires 2 arguments")
        return a + b + c + d + e + f

    def arith_subtraction(self, a, b):
        if a == None or b == None:
            Workflow.error("This command requires 2 arguments")
        return a - b

    def arith_multiplication(self, a, b):
        if a == None or b == None:
            Workflow.error("This command requires 2 arguments")
        return a * b

    def arith_division(self, a, b):
        if a == None or b == None:
            Workflow.error("This command requires 2 arguments")
        return a / b

    def arith_modulo(self, a, b):
        if a == None or b == None:
            Workflow.error("This command requires 2 arguments")
        return a % b

    def arith_sqrt(self, a):
        if a == None:
            Workflow.error("This command requires 1 argument")
        return a ** (0.5)

    def arith_cube(self, a):
        if a == None:
            Workflow.error("This command requires 1 argument")
        return a ** 3

    def arith_square(self, a):
        if a == None:
            Workflow.error("This command requires 1 argument")
        return a ** 2

    def arith_exponentiation(self, a, b):
        if a == None or b == None:
            Workflow.error("This command requires 2 arguments")
        return a ** b

class Kafe:
    def kafe_fetch(self, pkgn):
        acli_kafepy.fetch(pkgn)
    def kafe_upload(self, pkgn, pkgctn):
        acli_kafepy.upload(pkgn, pkgctn)
    def kafe_install(self, pkgn):
        acli_kafepy.install(self, pkgn)
