# Description module for Anvil CLI
# Descriptions are preconfigured here
# You can edit the descriptions
# by thecrimulo

# TODO: Finish all the descriptions

import acli_cconf

sumls = []
sumdict = {}

class Main:
    main_help = 'Displays the help index.'
    sumls.append(main_help)
    sumdict[acli_cconf.Main.main_help] = main_help
    main_exit = 'Exits the CLI'
    sumls.append(main_exit)
    sumdict[acli_cconf.Main.main_exit] = main_exit

class Kafe:
    kafe_main = 'Kafe Package Manager - KernlORG Anvil FTP Environment'
    sumls.append(kafe_main)
    sumdict[acli_cconf.Kafe.kafe_main] = kafe_main
    kafe_fetch = 'Downloads a package from KFTPS'
    sumls.append(kafe_fetch)
    sumdict[acli_cconf.Kafe.kafe_fetch] = kafe_fetch
    kafe_install = 'Installs a local package'
    sumls.append(kafe_install)
    sumdict[acli_cconf.Kafe.kafe_install] = kafe_install
    kafe_upload = 'Uploads a local package'
    sumls.append(kafe_upload)
    sumdict[acli_cconf.Kafe.kafe_upload] = kafe_upload
    kafe_remove = 'Removes a local package'
    sumls.append(kafe_remove)
    sumdict[acli_cconf.Kafe.kafe_remove] = kafe_remove
    kafe_takedown = 'Takes down and removes a package'
    sumls.append(kafe_takedown)
    sumdict[acli_cconf.Kafe.kafe_takedown] = kafe_takedown

class Arithmetic:
    arith_addition = 'Adds integers'
    sumls.append(arith_addition)
    sumdict[acli_cconf.Arithmetic.arith_addition] = arith_addition
    arith_subtraction = 'Subtracts integers'
    sumls.append(arith_subtraction)
    sumdict[acli_cconf.Arithmetic.arith_subtraction] = arith_subtraction
    arith_multiplication = 'Multiplies integers'
    sumls.append(arith_multiplication)
    sumdict[acli_cconf.Arithmetic.arith_multiplication] = arith_multiplication
    arith_division = 'Divides integers'
    sumls.append(arith_division)
    sumdict[acli_cconf.Arithmetic.arith_division] = arith_division
    arith_modulo = 'Remainder of a division of integers'
    sumls.append(arith_modulo)
    sumdict[acli_cconf.Arithmetic.arith_modulo] = arith_modulo
    arith_sqrt = 'Square root of an integer'
    sumls.append(arith_sqrt)
    sumdict[acli_cconf.Arithmetic.arith_sqrt] = arith_sqrt
    arith_cube = 'Cube of an integer'
    sumls.append(arith_cube)
    sumdict[acli_cconf.Arithmetic.arith_cube] = arith_cube
    arith_square = 'Square of an integer'
    sumls.append(arith_square)
    sumdict[acli_cconf.Arithmetic.arith_square] = arith_square
    arith_exponentiation = 'Exponents with two integers'
    sumls.append(arith_exponentiation)
    sumdict[acli_cconf.Arithmetic.arith_exponentiation] = arith_exponentiation

class IO:
    io_mdir1 = 'Create a new directory'
    io_mdir2 = 'Create a new directory'
    io_mdir3 = 'Create a new directory'
    io_rdir1 = 'Remove a directory'
    io_rdir2 = 'Remove a directory'
    io_rdir3 = 'Remove a directory'
    io_cdir1 = 'Moves to a directory'
    io_cdir2 = 'Moves to a directory'
    io_cdir3 = 'Moves to a directory'
    io_mfil1 = 'Makes a new file'
    io_mfil2 = 'Makes a new file'
    io_mfil3 = 'Makes a new file'
    io_rfil1 = 'Removes a file'
    io_rfil2 = 'Removes a file'
    io_rfil3 = 'Removes a file'
    io_efil = 'Edits a file'
