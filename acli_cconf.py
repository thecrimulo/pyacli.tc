# Command module for Anvil CLI
# Commands are preconfigured here
# You can edit the commands
# by thecrimulo

# TODO: Finish all the commands

cmdls = []

class Main:
    main_help = 'help'                  # Defaults to 'help'
    cmdls.append(main_help)
    main_exit = 'exit'                  # Defaults to 'exit'
    cmdls.append(main_exit)

class Kafe:
    kafe_main = 'kafe'                  # Defaults to 'kafe'
    cmdls.append(kafe_main)
    kafe_fetch = 'fetch'                # Defaults to 'fetch'
    cmdls.append(kafe_fetch)
    kafe_install = 'install'            # Defaults to 'install'
    cmdls.append(kafe_install)
    kafe_upload = 'upload'              # Defaults to 'upload'
    cmdls.append(kafe_upload)
    kafe_remove = 'rm'                  # Defaults to 'rm'
    cmdls.append(kafe_remove)
    kafe_takedown = 'takedown'          # Defaults to 'takedown'
    cmdls.append(kafe_takedown)

class Arithmetic:
    arith_addition = 'add'              # Defaults to 'add'
    cmdls.append(arith_addition)
    arith_subtraction = 'sub'           # Defaults to 'sub'
    cmdls.append(arith_subtraction)
    arith_multiplication = 'mult'       # Defaults to 'mult'
    cmdls.append(arith_multiplication)
    arith_division = 'div'              # Defaults to 'div'
    cmdls.append(arith_division)
    arith_modulo = 'mod'                # Defaults to 'mod'
    cmdls.append(arith_modulo)
    arith_sqrt = 'sqrt'                 # Defaults to 'sqrt'
    cmdls.append(arith_sqrt)
    arith_cube = 'cub'                  # Defaults to 'cub'
    cmdls.append(arith_cube)
    arith_square = 'sq'                 # Defaults to 'sq'
    cmdls.append(arith_square)
    arith_exponentiation = 'elev'       # Defaults to 'elev'
    cmdls.append(arith_exponentiation)

class IO:
    io_mdir1 = 'md'                     # Defaults to 'md'
    io_mdir2 = 'mkd'                    # Defaults to 'mkd'
    io_mdir3 = 'mkdir'                  # Defaults to 'mkdir'
    io_rdir1 = 'rd'                     # Defaults to 'rd'
    io_rdir2 = 'rmd'                    # Defaults to 'rmd'
    io_rdir3 = 'rmdir'                  # Defaults to 'rmdir'
    io_cdir1 = 'cd'                     # Defaults to 'cd'
    io_cdir2 = 'chd'                    # Defaults to 'chd'
    io_cdir3 = 'chdir'                  # Defaults to 'chdir'
    io_mfil1 = 'mk'                     # Defaults to 'mk'
    io_mfil2 = 'mkf'                    # Defaults to 'mkf'
    io_mfil3 = 'mkfile'                 # Defaults to 'mkfile'
    io_rfil1 = 'rm'                     # Defaults to 'rm'
    io_rfil2 = 'rmf'                    # Defaults to 'rmf'
    io_rfil3 = 'rmfile'                 # Defaults to 'rmfile'
    io_efil = 'bee'                     # Defaults to 'bee'
