# Anvil CLI - Anvil Command Line Interface
# Python 3
#
# By thecrimulo

## Imports
from ftplib import FTP
import os
import acli_utils
Workflow = acli_utils.Workflow()
import acli_cconf
MainLabels = acli_cconf.Main()
ArithmeticLabels = acli_cconf.Arithmetic()
KafeLabels = acli_cconf.Kafe()
import acli_dconf
MainDefs = acli_dconf.Main()
ArithmeticDefs = acli_dconf.Arithmetic()
KafeDefs = acli_dconf.Kafe()

## acli_pkg
import acli_pkg
acli_pkg.pkginit()

## Tracer
#import pdb; pdb.set_trace()

## ANSI Color Codes
# 30 40	Black
# 31 41 Red
# 32 42	Green
# 33 43	Yellow
# 34 44	Blue
# 35 45	Magenta
# 36 46	Cyan
# 37 47	White

class color:
    black = "\033[0;30;40m"
    red = "\033[0;31;40m"
    green = "\033[0;32;40m"
    yellow = "\033[0;33;40m"
    blue = "\033[0;34;40m"
    magenta = "\033[0;35;40m"
    cyan = "\033[0;36;40m"
    white = "\033[0;37;40m"
class boldcolor:
    black = "\033[1;30;40m"
    red = "\033[1;31;40m"
    green = "\033[1;32;40m"
    yellow = "\033[1;33;40m"
    blue = "\033[1;34;40m"
    magenta = "\033[1;35;40m"
    cyan = "\033[1;36;40m"
    white = "\033[1;37;40m"
class escapes:
    nl = "\n"
    s = " "

def clear():
    if os.name == "nt":
        os.system("cls")
    elif os.name == "posix":
        os.system("clear")
    else:
        os.system('echo "Boo!"')

## Variable Settings
usercolor = color.green
pathcolor = color.yellow

## First time configuration
clear()
while not os.path.exists("./cfg/regs.anv"):
    print(color.white + "This is your first time using Anvil CLI.")
    print("Welcome. This is a Command Line Interface, or CLI,")
    print("Here, you can run commands to do actions.")
    print("")
    clear()
    print("""
    KernlORG GEP Anvil CLI - GitLab Commit 9
    Terms and conditions of use
    ========================================
    The following files are included in the
    license:
    + acli_cconf.py
    + acli_dconf.py
    + acli_kafe.py
    + acli_main.py (RUNNING)
    + acli_pkg.py
    + acli_sconf.py
    + acli_utils.py
    + LICENSE (GNU GPL v3.0)
    + README.md
    ========================================
    1. Internet Usage conditions
    - Kafe, the package manager, uses Internet
    to establish a connection between the KFSS
    (KernlORG FTP Service Server) and the
    client. It's your responsability to take
    care of the usage.

    - When a Kafe Pointer / Kafe Symlink is
    downloaded, the acli_kafe.py library
    must use the 'pycurl' library to download
    and point to the online file, that will
    be storaged. This also uses internet.

    2. Malicious package warning
    - When downloading a package via KafePM
    or via manual installation, you always
    should check the code in the package,
    which may install virus, malware or
    some kind of spyware on your computer,
    because it implements Python 3.x to
    make libraries.

    3. Usage warning
    - We are not responsible of how your
    computer can be affected/damage because
    of the use of Anvil CLI, you can always
    check the source code, no malware/spyware.

    4. Free & open-source guarantee
    - This program costs no money and it is
    open source, the code is availiable in
    GitHub and GitLab, being GitLab the most
    used one and the recommended:
    + GitHub:
    # https://github.com/TheCrimulo/pyacli
    + GitLab (RECOMMENDED):
    # https://gitlab.com/TheCrimulo/pyacli
    ========================================
    Did you read the terms and you agree
    with them? (Y / N):
    """)
    response = input()
    if response == "N":
        exit()
    elif response == "n":
        exit()
    elif response == "Y":
        print("Thanks for accepting our terms.")
        print("We recommend you to read the license, too.")
    elif response == "y":
        print("Thanks for accepting our terms.")
        print("We recommend you to read the license, too.")
    clear()
    print("KernlORG GEP Anvil CLI - GitLab commit 9")
    print("We have to configure your userfile.")
    userconf = input("Username: $ ")
    cliconf = input("Host: $ ")
    passansw = input("Do you want a password? [y/n]:")
    if passansw == "y":
        passconf = input("Password: $ ")
    else:
        passconf = "TG9uZyBhZ28sIHR3byByYWNlcyBydWxlZCBvdmVyIGVhcnRoOiBIVU1BTlMgYW5kIE1PTlNURVJTLi4u"
    print("Creating directories.")
    print("Creating ./cfg")
    os.mkdir("./cfg")
    Workflow.success("Done.")
    print("Creating ./tmp")
    os.mkdir("./tmp")
    Workflow.success("Done.")
    print("Creating ./tmp/curl")
    os.mkdir("./tmp/curl")
    Workflow.success("Done.")
    print("Creating ./pkg")
    os.mkdir("./pkg")
    Workflow.success("Done.")
    print("Marking current directory (" + os.getcwd() + ") as home.")
    rh = open("./rh.anv", "w+")
    rh.close()
    Workflow.success("Done.")
    print("Creating and opening ./cfg/" + userconf + ".anv")
    fconf = open("./cfg/" + userconf + ".anv", "w+")
    print("Writing configuration")
    fconf.write(userconf + escapes.nl + cliconf + escapes.nl + passconf)
    fconf.close()
    Workflow.success("Done.")
    print("Creating userfile")
    fconfreg = open("./cfg/regs.anv", "a+")
    fconfreg.write(userconf + escapes.nl)
    fconfreg.close()
    Workflow.success("Done.")

## Init
clear()
ch = True
while ch:
    clear()
    print("".center(80))
    print(color.white + "Choose an user:".center(80))
    lines = [line.rstrip('\n') for line in open("./cfg/regs.anv", "r+")]
    for line in lines:
        print(line.center(80))
    chusr = input("$ ")
    if chusr in lines:
        ch = False
    else:
        continue
usrlns = [line.rstrip('\n') for line in open("./cfg/" + chusr + ".anv", "r+")]
if usrlns[2] != "TG9uZyBhZ28sIHR3byByYWNlcyBydWxlZCBvdmVyIGVhcnRoOiBIVU1BTlMgYW5kIE1PTlNURVJTLi4u":
    escpsw = True
    while escpsw:
        pword = input("This user needs a password: $ ")
        if pword == "rejectinput":
            escpsw = False
        elif pword != usrlns[2]:
            continue
        else:
            break
    if not escpsw:
        print(boldcolor.red + "Error: Password input denied." + boldcolor.white)
        exit(0)
else:
    pword = "TG9uZyBhZ28sIHR3byByYWNlcyBydWxlZCBvdmVyIGVhcnRoOiBIVU1BTlMgYW5kIE1PTlNURVJTLi4u"

## Initloop
clear()
cuser = usrlns[0]
chost = usrlns[1]
rhome = ":"
if os.path.exists("./rh.anv"):
    rhome = "[Home]:"
print("KernlORG GEP Anvil CLI - v0.01")
print("GNU GPL v3.0 - GNU General Program License.")
print("Run 'help' to display a command index.")
print("Run 'help:command' to display command help.")
print("")

while True:
    ## $cline example: user@host:~[Home]>
    cline = color.green + cuser + "@" + chost + ":" + color.yellow + os.getcwd() + rhome + ">"
    print(cline)
    cmd = input(color.white + "$ ")
    ## Check if package
    doer = acli_pkg.cllist.get(cmd)
    if doer != None:
        doer()
    ## Built-in commands
    if cmd == MainLabels.main_help:
        MainDefs.main_help()
    elif cmd == MainLabels.main_exit:
        MainDefs.main_exit()
    elif cmd.startswith("help:"):
        pl = cmd.split(':')
        MainDefs.main_helpcmd(pl[1])
    elif cmd.startswith(ArithmeticLabels.arith_addition + escapes.s):
        pm = cmd.split(' ')
        print(ArithmeticDefs.arith_addition(int(pm[1]), int(pm[2])))
    elif cmd.startswith(ArithmeticLabels.arith_subtraction + escapes.s):
        pm = cmd.split(' ')
        print(ArithmeticDefs.arith_subtraction(int(pm[1]), int(pm[2])))
    elif cmd.startswith(ArithmeticLabels.arith_multiplication + escapes.s):
        pm = cmd.split(' ')
        print(ArithmeticDefs.arith_multiplication(int(pm[1]), int(pm[2])))
    elif cmd.startswith(ArithmeticLabels.arith_division + escapes.s):
        pm = cmd.split(' ')
        print(ArithmeticDefs.arith_division(int(pm[1]), int(pm[2])))
    elif cmd.startswith(ArithmeticLabels.arith_modulo + escapes.s):
        pm = cmd.split(' ')
        print(ArithmeticDefs.arith_modulo(int(pm[1]), int(pm[2])))
    elif cmd.startswith(ArithmeticLabels.arith_sqrt + escapes.s):
        pm = cmd.split(' ')
        print(ArithmeticDefs.arith_sqrt(int(pm[1]), int(pm[2])))
    elif cmd.startswith(ArithmeticLabels.arith_cube + escapes.s):
        pm = cmd.split(' ')
        print(ArithmeticDefs.arith_cube(int(pm[1])))
    elif cmd.startswith(ArithmeticLabels.arith_square + escapes.s):
        pm = cmd.split(' ')
        print(ArithmeticDefs.arith_square(int(pm[1])))
    elif cmd.startswith(ArithmeticLabels.arith_exponentiation + escapes.s):
        pm = cmd.split(' ')
        print(ArithmeticDefs.arith_exponentiation(int(pm[1]), int(pm[2])))
    elif cmd.startswith(KafeLabels.kafe_main + escapes.s + KafeLabels.kafe_fetch + escapes.s):
        pm = cmd.split(' ')
        KafeDefs.kafe_fetch(pm[2])
    elif cmd.startswith(KafeLabels.kafe_main + escapes.s + KafeLabels.kafe_upload + escapes.s):
        pm = cmd.split(' ')
        if pm[3] != "gets":
            Workflow.error("[Kafe] Usage: 'kafe upload $pkgname gets $pkgfile'")
            continue
        KafeDefs.kafe_upload(pm[2], pm[4])
    elif cmd.startswith(KafeLabels.kafe_main + escapes.s + KafeLabels.kafe_install + escapes.s):
        pm = cmd.split(' ')
        KafeDefs.kafe_install(pm[2])
